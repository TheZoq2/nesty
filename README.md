# Nesty

A small crate to help generate human readable code from rust.

The primary interface is the `code!` macro which looks as follows:

```rust
code!{
    [0] "fn main() {";
    [1]     "println!(\"hello, world\");";
    [0] "}"
}
```

The bracketed numbers give the desired amount of indentation for the line.

Code blocks can also be nested, like so:

```rust
let if_expr = code!{
    [0] "if x > 0 {";
    [1]     "println(\"found one!\")";
    [0] "}"
}
code!{
    [0] "fn main() {";
    [1]     if_expr;
    [0] "}"
}
```
which will produce
```rust
fn main() {
    if x > 0 {
        println!("found one!");
    }
}
```

The code macro also supports strings, and vectors of strings which will be properly indented,
(vectors are assumed to be a vector of lines, strings are indented after each newline)

With the `diff_assert` feature, the crate also has a `assert_same_code` macro
which checks two strings for equality, and if they differ, prints a diff before panicing.

