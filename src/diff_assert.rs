use colored::Colorize;
use prettydiff::diff_chars;

pub fn check_and_print_diff(got: &str, expected: &str) -> bool {
    if got != expected {
        println!("{}:\n{}", "got".red(), got);
        println!("{}", "==============================================".red());
        println!("{}:\n{}", "expected".green(), expected);
        println!(
            "{}",
            "==============================================".green()
        );
        println!("{}", diff_chars(got, expected));
        println!(
            "{}",
            "==============================================".yellow()
        );
        false
    } else {
        true
    }
}
/// Take the diff of $got and $expected, if they differ, print a dif and panic Requires
/// `colored::Color` from the colored crate, and the prettydiff crate to be in scope
#[macro_export]
macro_rules! assert_same_code {
    ($got:expr, $expected:expr) => {
        if !nesty::diff_assert::check_and_print_diff($got, $expected) {
            panic!("Code mismatch")
        }
    };
}
